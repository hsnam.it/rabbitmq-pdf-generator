const express = require("express");
const bodyParser = require("body-parser");
const { PORT } = require("./config");
const mongoose = require("./database/mongoose");
const { connectToQueue } = require("./utils/rabbitmq-helper");
const { RABBITMQ_URL } = require('./config');
const app = new express();

app.use(bodyParser.json());

// Apply router by models
require("./routes")(app);

//Preference: https://expressjs.com/en/guide/error-handling.html
//Custom error handlers must be defined below require("./routes")(app)
// eslint-disable-next-line no-unused-vars
app.use(function (err, req, res, next) {
  res.status(500).send("Something wrong with the server, please try it later!");
  throw err;
});

//Open a mongodb connection
mongoose.connect();

//Connect to RabbitMQ
connectToQueue(RABBITMQ_URL, () => {
  app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
  });
});
