const mongoose = require("mongoose");

let Request = new mongoose.Schema({
  user_id: {
    type: String,
    required: true
  },
  urls: [
    {
      url: {
        type: String
      },
      download_succeed: {
        type: Boolean,
        default: null
      },
      downloadable_url: {
        type: String,
        default: ""
      }
    }
  ]
});

module.exports = mongoose.model("request", Request);
