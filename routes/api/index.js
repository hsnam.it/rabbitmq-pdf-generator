const ApiRouter = require("express").Router();

ApiRouter.use("/pdf-files", require("./pdf-files/pdf"));

ApiRouter.get("/", (req, res) => {
  return res.send({
    msg: "You know, for api"
  });
});

module.exports = ApiRouter;
