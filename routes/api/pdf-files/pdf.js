const express = require("express");
const pdfRouter = express.Router();
const { publishToQueue } = require("../../../utils/rabbitmq-helper");
const Request = require("../../../database/models/request");
const User = require("../../../database/models/user");
const { PDF_OUTPUTS } = require('../../../config');

pdfRouter.post("/", runAsyncWrapper(async (req, res) => {
  const webpageUrls = req.body.urls;
  if (!webpageUrls || webpageUrls.length <= 0) {
    return res.status(422).send("urls value is required");
  }
  const formatedWebPageUrls = formatUrlsArr(webpageUrls);

  const email = req.body.email;
  if (!email) {
    return res.status(422).send("email value is required");
  }

  if (validateEmail(email) === false) {
    return res.status(422).send("email address is invalid");
  }

  let userId;
  const userInfo = await User.findOne({ email: email });
  if (!userInfo) {
    let newUser = await User.create({ email: email });
    userId = newUser._id;
  } else {
    userId = userInfo._id;
  }

  const newRequest = await Request.create({
    user_id: userId,
    urls: formatedWebPageUrls
  });

  formatedWebPageUrls.forEach((page, index) => {
    publishToQueue(
      "create_pdf_file",
      JSON.stringify({
        url: page.url,
        requestId: newRequest._id,
        is_last_url: index === formatedWebPageUrls.length - 1 ? true : false
      })
    );
  });

  return res
    .status(200)
    .send(`The request is processing, the result will be sent to ${email}`);
}));

pdfRouter.get("/download", runAsyncWrapper(async (req, res) => {
  const filePath = `${PDF_OUTPUTS}/${req.query.folder}/${encodeURIComponent(req.query.target)}`;
  return res.download(filePath);
}));

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

function formatUrlsArr(urlsArr) {
  let result = [];
  urlsArr.forEach(url => {
    result.push({
      url: url
    });
  });
  return result;
}

function runAsyncWrapper(callback) {
  return function (req, res, next) {
    callback(req, res, next).catch(next);
  };
}

module.exports = pdfRouter;
