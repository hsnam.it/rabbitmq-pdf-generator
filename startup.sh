#!/bin/bash
#pm2 start app.js --node-args="--inspect-brk" --watch
pm2 start app.js
pm2 start ./workers/mailer.js -i max #--node-args="--inspect-brk" --watch
pm2 start ./workers/pdf-generator.js -i max #--node-args="--inspect-brk" --watch