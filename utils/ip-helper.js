'use strict';
const os = require('os');

function getLocalIPv4() {
    const ifaces = os.networkInterfaces();

    let IPs = {};
    Object.keys(ifaces).forEach(function (ifname) {
        let alias = 0;

        ifaces[ifname].forEach(function (iface) {
            if ('IPv4' !== iface.family || iface.internal !== false) {
                // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
                return;
            }

            if (alias >= 1) {
                // this single interface has multiple ipv4 addresses
                IPs[ifname] = iface.address;
            } else {
                // this interface has only one ipv4 adress
                IPs[ifname] = iface.address;
            }
            ++alias;
        });
    });

    const allowInterfaces = ['ethernet', 'ens33'];
    let ipV4 = "";
    for (const keyInterface in IPs) {
        if (allowInterfaces.includes(keyInterface.toString().toLowerCase())) {
            ipV4 = IPs[keyInterface];
        }
    }

    return (ipV4.length > 0) ? ipV4 : '127.0.0.1';
}

module.exports = {
    getLocalIPv4: getLocalIPv4
}
