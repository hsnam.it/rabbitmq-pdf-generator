const amqp = require("amqplib/callback_api");

let _connection = null;
let _channel = null;

function connectRabbitMq(amqpUrl, callback) {
  amqp.connect(amqpUrl, (err, connection) => {
    if (err) {
      throw err;
    }

    _connection = connection;
    _connection.on("error", err => {
      if (err.message !== "Connection closing") {
        console.error("[AMQP] conn error", err.message);
      }
    });

    _connection.on("close", () => {
      console.error("[AMQP] reconnecting");
      return setTimeout(connectRabbitMq(amqpUrl, callback), 1000);
    });

    console.log("[AMQP] connected");

    //Define a channel
    _connection.createConfirmChannel((error, channel) => {
      if (error) {
        throw error;
      }

      _channel = channel;

      if (callback) {
        callback();
      }
    });
  });
}

function publishMessage(queueName, data) {
  if (_connection === null) return;

  _channel.assertQueue(queueName);
  //This option is to force workers consume message one by one
  _channel.prefetch(1);
  _channel.sendToQueue(queueName, Buffer.from(data));
}

function consumeMessage(queueName, callback) {
  if (_connection === null) return;

  _channel.assertQueue(queueName);
  //This option is to force workers consume message one by one
  _channel.prefetch(1);
  _channel.consume(queueName, msg => {
    callback(_channel, msg);
  });
}

module.exports = {
  connectToQueue: connectRabbitMq,
  publishToQueue: publishMessage,
  consumeMessage: consumeMessage
};
