const { connectToQueue, consumeMessage } = require("../utils/rabbitmq-helper");
const nodemailer = require("nodemailer");
const Request = require("../database/models/request");
const User = require("../database/models/user");
const mongoose = require("../database/mongoose");
const { RABBITMQ_URL, MAIL_ACCOUNT, MAIL_PASSWORD, PORT } = require("../config");
const { getLocalIPv4 } = require('../utils/ip-helper');
const path = require('path');

mongoose.connect();

connectToQueue(RABBITMQ_URL, () => {
  const queueToListen = "send_email";
  consumeMessage(queueToListen, async (channel, msg) => {
    try {
      const msgJson = JSON.parse(msg.content);
      const request = await Request.findById(msgJson.requestId);
      if (!request) { channel.ack(msg); return; }

      const user = await User.findById(request.user_id);
      if (!user) { channel.ack(msg); return; }

      const transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
          user: MAIL_ACCOUNT,
          pass: MAIL_PASSWORD
        }
      });

      const downloadableLinks = Array.from(request.urls, (obj) => {
        if (obj.download_succeed === true)
          return convertAbsolutePathToDownloadable(obj.downloadable_url);
      });

      const mailOptions = {
        from: MAIL_ACCOUNT,
        to: user.email,
        subject: "Your files from PDF GENERATOR are ready",
        text: downloadableLinks.join("\r\n")
      };

      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          throw error;
        }
        console.log(`Email sent to ${user.email}: ${info.response}`);
      });

      channel.ack(msg);
    } catch (err) {
      channel.ack(msg);
      throw err;
    }
  });
});


function convertAbsolutePathToDownloadable(target) {
  const splitFolderAndFile = target.split(path.sep);
  const arrLength = splitFolderAndFile.length;
  if (arrLength <= 0) return target;

  return `http://${getLocalIPv4()}:${PORT}/api/pdf-files/download?folder=${splitFolderAndFile[arrLength - 2]}&target=${splitFolderAndFile[arrLength - 1]}`;
}