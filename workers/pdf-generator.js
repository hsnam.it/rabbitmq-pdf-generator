const { connectToQueue, publishToQueue, consumeMessage } = require("../utils/rabbitmq-helper");
const { RABBITMQ_URL } = require("../config");
const { mkdirByPathSync } = require('../utils/mkdir-helper');

const mongoose = require("../database/mongoose");
const Request = require("../database/models/request");

const puppeteer = require("puppeteer");
const fs = require("fs");
const { resolve } = require('path');

mongoose.connect();

//Connect to RabbitMQ
connectToQueue(RABBITMQ_URL, () => {
    const queueToListen = "create_pdf_file";
    const queueToPublish = "send_email";
    consumeMessage(queueToListen, async (channel, msg) => {
        const msgJson = JSON.parse(msg.content);
        const url = msgJson.url;
        const requestId = msgJson.requestId;
        const isLastUrl = msgJson.is_last_url;

        //Generate PDF here
        try {
            const pdf = await generatePdf(url);
            const parentFolder = `../pdf-outputs/${requestId}`;
            mkdirByPathSync(parentFolder);

            const fileName = `${encodeURIComponent(url)}.pdf`;
            const filePath = `${parentFolder}/${fileName}`;

            fs.writeFile(filePath, pdf, async (err) => {
                if (err) throw `File creating: ${err}`;

                //Update download_succeed and downloadable_url
                const queryResult = await Request.findOneAndUpdate(
                    { _id: requestId, urls: { $elemMatch: { url: url } } },
                    { $set: { "urls.$.download_succeed": true, "urls.$.downloadable_url": resolve(filePath) } });
                if (!queryResult || !isLastUrl) return;

                publishToQueue(
                    queueToPublish,
                    JSON.stringify({ requestId: requestId })
                );
            });

        } catch (exp) {
            const queryResult = Request.findOneAndUpdate(
                { _id: requestId, urls: { $elemMatch: { url: url } } },
                { $set: { "urls.$.download_succeed": false } }
            );

            if (!queryResult) return;

            //Acknowledge old message and republish a message to queue to retry downloading
            channel.ack(msg);
            let retryCount = msgJson.retryCount === undefined ? 0 : msgJson.retryCount;
            if (retryCount < 3) {
                msgJson.retryCount = retryCount + 1;
                publishToQueue(queueToListen, JSON.stringify(msgJson));
            }

            throw exp;
        }

        //Acknowledge the given message
        channel.ack(msg);
    });
});

async function generatePdf(url) {
    const browser = await puppeteer.launch({ headless: true });
    const page = await browser.newPage();
    await page.goto(url, { waitUntil: "networkidle0" });
    const pdf = await page.pdf({ format: "A4" });
    await browser.close();
    return pdf;
}
